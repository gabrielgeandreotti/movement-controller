﻿public static class Extensions
{
    public static float Clamp(this float value, float min, float max)
    {
        if (value < min)
            value = min;
        else if (value > max)
            value = max;
        return value;
    }
}