﻿using UnityEngine;

public class KeyboardInput : MonoBehaviour, IMoveInput
{
    //----------------------------------------------------------------------------------------------------
    // Constants
    //----------------------------------------------------------------------------------------------------
    private const string HorizontalAxis = "Horizontal";
    private const string VerticalAxis = "Vertical";
    
    //----------------------------------------------------------------------------------------------------
    // Non-serialized fields
    //----------------------------------------------------------------------------------------------------
    private Vector2 _input;
    
    //----------------------------------------------------------------------------------------------------
    // Properties
    //----------------------------------------------------------------------------------------------------
    public Vector2 MoveVector
    {
        get
        {
            _input.Set(Input.GetAxisRaw(HorizontalAxis), Input.GetAxisRaw(VerticalAxis));
            _input.Normalize();
            return _input;
        }
    }
}