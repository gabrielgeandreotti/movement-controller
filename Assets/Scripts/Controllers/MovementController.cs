﻿using UnityEngine;

public class MovementController : MonoBehaviour
{
    //-------------------------------------------------------------------------------------------------------------
    // Serialized fields
    //-------------------------------------------------------------------------------------------------------------
    [Header("Cached Components")]
    [SerializeField] private Transform _transform;
    
    [Header("Settings")]
    [SerializeField] private float _speed;
    
    //-------------------------------------------------------------------------------------------------------------
    // Non-serialized fields
    //-------------------------------------------------------------------------------------------------------------
    private IMoveInput _input;
    private Vector2 _localPosition;
    
    //-------------------------------------------------------------------------------------------------------------
    // Properties
    //-------------------------------------------------------------------------------------------------------------
    public float Speed => _speed;

    //-------------------------------------------------------------------------------------------------------------
    // Unity events
    //-------------------------------------------------------------------------------------------------------------
    private void Start()
    {
        TryGetComponent(out _input);
        _localPosition = _transform.localPosition;
    }

    private void Update()
    {
        if (_input == null) return;
        _localPosition += Time.deltaTime * _speed * _input.MoveVector;
        _transform.localPosition = _localPosition;
    }

    private void OnValidate()
    {
        if (_speed < 1)
            _speed = 1;
    }
}